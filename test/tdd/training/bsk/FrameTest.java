package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class FrameTest {

	@Test
	//Test user story 1
	public void testGetThrows() throws Exception{
		Frame f = new Frame(2,4);
		assertEquals(2, f.getFirstThrow());
		assertEquals(4, f.getSecondThrow());
	}
	
	@Test
	//Test user story 2
	public void testGetScore() throws Exception{
		int firstThrow = 2;
		int secondThrow = 4;
		int score = firstThrow + secondThrow;
		Frame f = new Frame(firstThrow,secondThrow);
		assertEquals(score, f.getScore());
		
	}
	
	@Rule
    public ExpectedException exception = ExpectedException.none();
	@Test
	// Test user story 2 exception if score>10
	public void testGetScoreException() throws Exception{
		exception.expect(BowlingException.class);
        exception.expectMessage("Lo score del frame � maggiore di 10");
        int i;
		Frame frame = new Frame(2, 9);
		i = frame.getScore();
	}
	
	@Test
	// Test user story 3
	public void testGetFrameAt() throws Exception{
		Game game = new Game();
		Frame frame = new Frame(2, 4);
		int index = 0;
		game.addFrame(frame);
		assertEquals(frame, game.getFrameAt(index));
	}
	
	@Rule
    public ExpectedException exception2 = ExpectedException.none();
	@Test
	// Test user story 3 exception if frame>10
	public void testGame() throws Exception{
		exception.expect(BowlingException.class);
        exception.expectMessage("Il Game pu� contenere massimo 10 Frame");
		
		Game game = new Game();;
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.addFrame(new Frame(3,9));
		game.addFrame(new Frame(3,6));

	}

	@Test
	//Test user story 4
	public void testGameScore() throws Exception{
		Game game = new Game();
		int score = 81;

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	//Test user story 5 is spare
	public void testIsSpare() throws Exception{
		Frame f = new Frame(1,9);
		assertTrue(f.isSpare());
	}
	
	@Test
	// Test user story 5 spare
	public void testGameScoreWithSpare() throws Exception{
		int score = 88;
		Game game = new Game();
		
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
				
		assertEquals(score, game.calculateScore());
	}

	@Test
	//Test user story 6 is strike
	public void testIsStrike() throws Exception{
		Frame f = new Frame(10,0);
		assertTrue(f.isStrike());
	}
	
	@Test
	// Test user story 6 game with strike
	public void testGameWithStrike() throws Exception{
		int score = 94;
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
				
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// Test user story 7 game with strike
	public void testGameWithStrikeAndSpare() throws Exception{
		int score = 103;
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
				
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// Test user story 8 game with multiple strike
	public void testGameWithMultipleStrike() throws Exception{
		int score = 112;
		Game game = new Game();
		
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
				
		assertEquals(score, game.calculateScore());
	}

	@Test
	// Test user story 9 game with multiple spare
	public void testGameWithMultipleSpare() throws Exception{
		int score = 98;
		Game game = new Game();
		
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
				
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// Test user story 10
	public void testGetFirstBonusThrow() throws Exception{
		int bonusThrow = 7;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(bonusThrow);

		assertEquals(bonusThrow, game.getFirstBonusThrow());
	}
	
	@Test
	// Test user story 10 game with spare as last
	public void testGameWithSpareAsLast() throws Exception{
		int firstbonusThrow = 7;
		int score = 90;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(firstbonusThrow);

		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// Test user story 11
	public void testGetSecondBonusThrow() throws Exception{
		int firstbonusThrow = 7;
		int secondbonusThrow = 2;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstbonusThrow);
		game.setSecondBonusThrow(secondbonusThrow);

		assertEquals(secondbonusThrow, game.getSecondBonusThrow());
	}
	
	@Test
	// Test user story 11 game with strike as last
	public void testGameWithstrikeAsLast() throws Exception{
		int firstbonusThrow = 7;
		int secondbonusThrow = 2;
		int score = 92;
		Game game = new Game();

		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstbonusThrow);
		game.setSecondBonusThrow(secondbonusThrow);

		assertEquals(score, game.calculateScore());
	}
	
	@Test
	// Test user story 12 game with all strikes 
	public void testGameBestScore() throws Exception{
		int firstbonusThrow = 10;
		int secondbonusThrow = 10;
		int score = 300;
		Game game = new Game();

		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstbonusThrow);
		game.setSecondBonusThrow(secondbonusThrow);

		assertEquals(score, game.calculateScore());
	}
	
}
