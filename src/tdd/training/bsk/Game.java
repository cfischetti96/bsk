package tdd.training.bsk;

public class Game {
	
	protected Frame[] frameList;
	protected int i; // frame index
	protected int firstbonusThrow;
	protected int secondbonusThrow;
	

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		this.frameList = new Frame[10];
		this.i = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(i < 10) {
		this.frameList[this.i] = frame;
		i++;
		}
		else {
			throw new BowlingException("Il Game pu� contenere massimo 10 Frame");
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return this.frameList[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstbonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondbonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return this.firstbonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return this.secondbonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int n = 0; n < frameList.length; n++) {
			//spare in ultima posizione
			if(frameList[n].isSpare() && n == 9){
				score = score + frameList[n].getScore() + this.firstbonusThrow;
				
			}
			//controlla se il lancio � uno spare
		    else if(frameList[n].isSpare()) {
				frameList[n].setBonus(frameList[n+1].getFirstThrow());
				score = score + frameList[n].getScore();
			}


			//strike in penultima posizione
			else if(frameList[n].isStrike() && n == 8) {
				frameList[n].setBonus(frameList[n+1].getScore() + this.firstbonusThrow);
				score = score + frameList[n].getScore();
				
			}
			//strike in ultima posizione
			else if(frameList[n].isStrike() && n == 9) {
				score = score + frameList[n].getScore() + this.firstbonusThrow + this.secondbonusThrow;
			}
			else if(frameList[n].isStrike()) {
				//controlla se il lancio successivo � uno strike
				if(frameList[n+1].isStrike()) {
					//se � uno strike aggiorna il bonus
					frameList[n].setBonus(frameList[n+1].getScore() + frameList[n+2].getFirstThrow());
					score = score + frameList[n].getScore();
				}
				//se non � uno strike aggiunge lo score del frame al bonus
				else {
					frameList[n].setBonus(frameList[n+1].getScore());
					score = score + frameList[n].getScore();
				}			
			}
			else score = score + frameList[n].getScore();
		}
		return score;	
	}

}
